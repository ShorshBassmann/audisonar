# Anleitung zur Erstellung deiner ersten Electron App

[Link zur Offiziellen Anleitung](https://electronjs.org/docs/tutorial/first-app)

## Node
Installiere [Node JS](https://nodejs.org/en/), damit du den `npm` nutzen kannst.

## GIT
Installiere [git](https://git-scm.com/downloads) um das Repo zu Klonen.

## Clonen
wechsel in das Verzeichniss in welchem du das Projekt speichern willst. Führe `$git clone https://gitlab.com/ShorshBassmann/audisonar.git` aus.

## Applikation ausführen
Wechsel in die *audisonar* Directory mit `$cd audisonar`.

Lade alle benötigten Node-Module herunter mit `$npm install`.

Starte die Applikation mit `npm start`.

